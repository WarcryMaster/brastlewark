/// <reference path="../typings/tsd.d.ts" />

module Controllers{
  export class GnomesCtrl{
    public $inject = ['Gnomes'];
    gnomes: Models.Gnome[];
    constructor(
      Gnomes: Services.IGnomesService
    ){
      var self = this;
      Gnomes.fetchData().success(function(response) {
        self.gnomes = response.Brastlewark; 
        Gnomes.pushData(self.gnomes);          
      })
    }
  }

  export class GnomesDetailCtrl{
    public $inject = ['Gnomes', '$stateParams'];
    gnome: Models.Gnome;

    constructor(
      Gnomes: Services.IGnomesService,
      $stateParams: ng.ui.IStateParamsService
    ){
      this.gnome = Gnomes.getGnome($stateParams.gnomeid);
    }
  }
}

angular.module('starter.controllers', [])

  .controller('GnomesCtrl', Controllers.GnomesCtrl)

  .controller('GnomesDetailCtrl', Controllers.GnomesDetailCtrl);
