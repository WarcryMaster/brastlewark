module Models{
    export class Gnome{
        public id: string;
        public name: string;
        public thumbnail: string;
        public age: number;
        public weight: number;
        public height: number;
        public hair_color: string;
        public professions: string[];
        public friends: string[];
        constructor(
            id: string,
            name: string,
            thumbnail: string,
            age: number,
            weight: number,
            height: number,
            hair_color: string,
            professions: string[],
            friends: string[]
        ){
            this.id = id;
            this.name = name;
            this.thumbnail = thumbnail;
            this.age = age;
            this.weight = weight;
            this.height = height;
            this.hair_color = hair_color;
            this.professions = professions;
            this.friends = friends;
        }
    }
}