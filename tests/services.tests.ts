/// <reference path="../typings/tsd.d.ts" />
/// <reference path="../src/app.ts" />
/// <reference path="../src/controllers.ts" />
/// <reference path="../src/filters.ts" />
/// <reference path="../src/models.ts" />
/// <reference path="../src/services.ts" />

describe("Gnome Service", function() {
    var gnomesService = new Services.Gnomes({});
    var mockedGnome1 = new Models.Gnome(
                            '1', 
                            'Jason Bourne', 
                            "", 
                            180, 
                            78.5646, 
                            156.2365, 
                            'blue', 
                            ['Cooker', 'Programmer'], 
                            ['Ethan Hawk', 'Bruce Willis']);
    var mockedGnome2 = new Models.Gnome(
                            '2', 
                            'Matt Damon', 
                            "", 
                            185, 
                            88.5646, 
                            183.2365, 
                            'red', 
                            ['Actor', 'Productor'], 
                            ['Leonardo Di Caprio', 'Matt LeBlanc']);
    var mockGnomes = [mockedGnome1, mockedGnome2];

    beforeEach(function(){
        gnomesService.gnomes = null;
    });

    describe("service has functions defined", function() {

        it("exists", function() {
            expect(gnomesService).toBeDefined();
        });

        it("has getGnome", function() {
            var getGnomeFunction = gnomesService['getGnome'];
            expect(typeof(getGnomeFunction)).toBe('function');
        });

        it("has fetchData", function() {
            var fetchDataFunction = gnomesService['fetchData'];
            expect(typeof(fetchDataFunction)).toBe('function');
        });

        it("has pushData", function() {
            var pushDataFunction = gnomesService['pushData'];
            expect(typeof(pushDataFunction)).toBe('function');
        });
    })

    describe("method getGnome", function() {

        it("there are not data", function() {
            expect(gnomesService.getGnome("2")).toBe(null);
        });

        it("there are gnomes", function() {
            gnomesService.gnomes = mockGnomes;
            expect(gnomesService.getGnome("2")).toEqual(mockedGnome2);
        });
    })

    describe("method pushData", function() {

        it("insert gnomes", function() {
            expect(gnomesService.gnomes).toBe(null);
            gnomesService.pushData(mockGnomes);
            expect(gnomesService.gnomes).toBe(mockGnomes);
        });
    })
});