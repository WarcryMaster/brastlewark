/// <reference path="../typings/tsd.d.ts" />
var Controllers;
(function (Controllers) {
    var GnomesCtrl = (function () {
        function GnomesCtrl(Gnomes) {
            this.$inject = ['Gnomes'];
            var self = this;
            Gnomes.fetchData().success(function (response) {
                self.gnomes = response.Brastlewark;
                Gnomes.pushData(self.gnomes);
            });
        }
        return GnomesCtrl;
    }());
    Controllers.GnomesCtrl = GnomesCtrl;
    var GnomesDetailCtrl = (function () {
        function GnomesDetailCtrl(Gnomes, $stateParams) {
            this.$inject = ['Gnomes', '$stateParams'];
            this.gnome = Gnomes.getGnome($stateParams.gnomeid);
        }
        return GnomesDetailCtrl;
    }());
    Controllers.GnomesDetailCtrl = GnomesDetailCtrl;
})(Controllers || (Controllers = {}));
angular.module('starter.controllers', [])
    .controller('GnomesCtrl', Controllers.GnomesCtrl)
    .controller('GnomesDetailCtrl', Controllers.GnomesDetailCtrl);
